from sys import stdin as sys_stdin

result: float = 42

for raw_input_line in sys_stdin:
    # Убираем переносы строки командой rstrip()
    input_line = raw_input_line.rstrip()
    # Проверяем, являются ли все символы строки цифрами, а также не является ли строка концом файла
    if all(char.isdigit() for char in input_line) is True and input_line != "":
        # Прибавляем элемент прогрессии 1 / num
        result += 1 / int(input_line)

print(f"Сумма ряда: {result:.5f}")
