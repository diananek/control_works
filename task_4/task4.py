from random import randint, uniform
from time import time
import matplotlib.pyplot as plt
from numpy import poly1d as np_poly1d, polyfit as np_polyfit
from task_utils import matrix_multiplication_calculation

# Минимальная размерность матрицы.
dimension_min = 10**2
# Максимальная размерность матрицы.
dimension_max = 10**4
# Шаг изменения размерности матрицы.
dimension_step = 10**3

# Массив размерностей матриц.
dimensions = []
# Массивы результирующих времен умножения матриц.
calculation_times_int = []
calculation_times_float = []
for dimension in range(dimension_min, dimension_max, dimension_step):
    # dimension - размерность матрицы i * j, потому примем матрицу за квадратную.
    # Тогда значения i и j одинаковые и равняются корню квадратному от dimension.
    # Заполняем матрицы целочисленными значениями.
    matrix_a = [[randint(1, 10) for j in range(int(dimension**0.5))] for i in range(int(dimension**0.5))]
    matrix_b = [[randint(1, 10) for j in range(int(dimension**0.5))] for i in range(int(dimension**0.5))]


    # Массив результатов времени нескольких подсчетов.
    temp = []

    for _ in range(3):
        timer_start = time()
        # Умножение двух матриц.
        matrix_multiplication_calculation(matrix_a, matrix_b)
        timer_stop = time()
        temp.append(timer_stop - timer_start)

    # Занесение результирующего значения подсчетов времени.
    calculation_times_int.append(sum(temp) / 3)
    temp = []
    # Заполняем матрицы вещественными значениями.
    matrix_a = [[uniform(1, 10) for j in range(int(dimension ** 0.5))] for i in range(int(dimension ** 0.5))]
    matrix_b = [[uniform(1, 10) for j in range(int(dimension ** 0.5))] for i in range(int(dimension ** 0.5))]

    for _ in range(3):
        timer_start = time()
        # Умножение двух матриц.
        matrix_multiplication_calculation(matrix_a, matrix_b)
        timer_stop = time()
        temp.append(timer_stop - timer_start)


    # Занесение результирующего значения подсчетов времени.
    calculation_times_float.append(sum(temp) / 3)
    # Занесение текущей размерности матрицы.
    dimensions.append(dimension)

# Визуальное отображение графика из произведенных расчетов.
plt.figure(figsize=(10,6))

plt.subplot(211)
plt.plot(dimensions, calculation_times_float, "o")
trend_float = np_poly1d(np_polyfit(dimensions, calculation_times_float, 3))
plt.plot(dimensions, trend_float(dimensions), c="blue", label=f"Тренд вычислений float()")

plt.legend()
plt.xlabel("Количество элементов в списке, *10^4")
plt.ylabel("Время вычисления, с.")

plt.subplot(212)
plt.plot(dimensions, calculation_times_int, "o")
trend_float = np_poly1d(np_polyfit(dimensions, calculation_times_int, 3))
plt.plot(dimensions, trend_float(dimensions), c="red", label=f"Тренд вычислений int()")

plt.legend()
plt.xlabel("Количество элементов в списке, *10^4")
plt.ylabel("Время вычисления, с.")

plt.show()