from task_utils import saxpy_calculation

# Определяем тип входных данных, если тип неверный - выдаем ошибку.
input_type_choice = input("Какого типа будут входные значения: int или float?\t").lower()
if "int" or "INT" or "Int" in input_type_choice:
    input_type = int
elif "float" or "FLOAT" or "Float" in input_type_choice:
    input_type = float
else:
    raise TypeError

# Вводим значения векторов и скаляра в соответствии с выбранным типом входных данных
vector_x = list(map(input_type, input("Введите координаты вектора X через пробел:\t").split()))
vector_y = list(map(input_type, input("Введите координаты вектора Y через пробел:\t").split()))
scalar_a = input_type(input_type(input("Введите значение скаляра A:\t")))

# Функцией ZIP мы связываем координаты векторо X и Y, по сути обрабатывая пары координат за один шаг цикла.
calculated_vector = saxpy_calculation(vector_x, vector_y, scalar_a)

print(f"Результат вычислений алгоритма SAXPY - {', '.join(calculated_vector)}")
